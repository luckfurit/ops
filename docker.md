# 容器技术

[toc]

## 安装部署：

### docker安装

```shell
# 拷贝安装包到目标主机
[root@localhost ~]# tar cf docker.tar docker
[root@localhost ~]# scp docker.tar 192.168.1.31:./
#---------------------------------------------------------------------#
[root@docker ~]# echo 'net.ipv4.ip_forward = 1' >>/etc/sysctl.conf
[root@docker ~]# sysctl -p
net.ipv4.ip_forward = 1
[root@docker ~]# tar xf docker.tar
[root@docker ~]# yum install -y docker/*
[root@docker ~]# systemctl enable --now docker
[root@docker ~]# docker version
Client: Docker Engine - Community
 Version:           20.10.10
 ... ...
Server: Docker Engine - Community
 Engine:
  Version:          20.10.10
```

### 配置镜像加速

```shell
[root@docker ~]# vim /etc/docker/daemon.json
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "registry-mirrors": ["http://hub-mirror.c.163.com"],
    "insecure-registries":[]
}
[root@docker ~]# systemctl restart docker
[root@docker ~]# docker info
... ...
 Insecure Registries:
  127.0.0.0/8
 Registry Mirrors:
  https://hub-mirror.c.163.com/
 Live Restore Enabled: false
```

## 镜像管理&容器管理

### 镜像管理命令

| 镜像管理命令                                      | 说明                                       |
| :------------------------------------------------ | :----------------------------------------- |
| docker  images                                    | 查看本机镜像                               |
| docker  search  镜像名称                          | 从官方仓库查找镜像                         |
| docker  pull  镜像名称:标签                       | 下载镜像                                   |
| docker  push  镜像名称:标签                       | 上传镜像                                   |
| docker  save  镜像名称:标签  -o  备份镜像名称.tar | 备份镜像为tar包                            |
| docker  load -i  备份镜像名称                     | 导入备份的镜像文件                         |
| docker  rmi  镜像名称:标签                        | 删除镜像（必须先删除该镜像启动的所有容器） |
| docker  history  镜像名称:标签                    | 查看镜像的制作历史                         |
| docker  inspect  镜像名称:标签                    | 查看镜像的详细信息                         |
| docker  tag  镜像名称:标签  新的镜像名称:新的标签 | 创建新的镜像名称和标签                     |

```shell
# ----------------------以下操作必须在一台可以访问互联网的机器上执行---------------------------
# 查看镜像
[root@docker ~]# docker images

# 搜索镜像
[root@docker ~]# docker search centos

# 获取标签（国内可能无法访问）
[root@docker ~]# curl -s https://hub.docker.com/v1/repositories/library/centos/tags

# 下载镜像
[root@docker ~]# docker pull busybox:latest

# 备份镜像到 tar 包
[root@docker ~]# docker save busybox:latest -o busybox.tar

# 删除镜像，不能删除已经创建容器的镜像
[root@docker ~]# docker rmi busybox:latest

# 从备份包导入镜像
[root@docker ~]# docker load -i busybox.tar

# 查看镜像的详细信息
[root@docker ~]# docker inspect busybox:latest

# 查看镜像的历史信息
[root@docker ~]# docker history busybox:latest

# 给镜像添加新的名词和标签
[root@docker ~]# docker tag busybox:latest myos:latest
```

### 容器管理命令

| 容器管理命令                                | 说明                                            |
| ------------------------------------------- | ----------------------------------------------- |
| docker  run  -it(d) 镜像名称:标签  启动命令 | 创建启动并进入一个容器                          |
| docker  ps                                  | 查看容器 -a 所有容器，包含未启动的，-q 只显示id |
| docker  rm  容器ID                          | -f 强制删除                                     |
| docker  start\|stop\|restart  容器id        | 启动、停止、重启容器                            |
| docker  exec  -it  容器id  启动命令         | 在容器内执行命令                                |
| docker  cp  本机文件路径  容器id:容器内路径 | 把本机文件拷贝到容器内（上传）                  |
| docker  cp  容器id:容器内路径  本机文件路径 | 把容器内文件拷贝到本机（下载）                  |
| docker  inspect  容器ID                     | 查看容器的详细信息                              |
| docker  logs  容器ID                        | 查看容器日志                                    |
| docker  info                                | 查看容器的配置信息                              |
| docker  version                             | 查看服务器与客户端版本                          |

```shell
# 在前台启动容器
[root@docker ~]# docker run -it busybox:latest
/ # ctrl+p, ctrl+q # 使用快捷键退出，保证容器不关闭

# 在后台启动容器
[root@docker ~]# docker run -itd httpd:latest
58c4658ebcb723e906dac874418f8f9d1e70a787ec4704eeed9d5e0ae0274b7a

# 查看容器
[root@docker ~]# docker ps
CONTAINER ID   IMAGE           COMMAND                  CREATED         STATUS
58c4658ebcb7   busybox:httpd   "/usr/sbin/httpd -DF…"   5 seconds ago   Up 2 seconds

# 只查看id
[root@docker ~]# docker ps -q
58c4658ebcb7

# 查看所有容器，包含未启动的
[root@docker ~]# docker ps -a
CONTAINER ID   IMAGE            COMMAND                 CREATED         STATUS
23e3f9520380   busybox:latest   "/bin/sh"               5 minutes ago   Exited (0)
58c4658ebcb7   httpd:latest     "/usr/sbin/httpd -DF…"  5 minutes ago   Up 3 minutes

# 查看所有容器id，包含未启动的
[root@docker ~]# docker ps -aq
23e3f9520380
58c4658ebcb7

# 启动、停止、重启容器
[root@docker ~]# docker start   de46e6254efd
[root@docker ~]# docker stop    9cae0af944d8
[root@docker ~]# docker restart 9cae0af944d8

# 查看容器详细信息
[root@docker ~]# docker inspect 9cae0af944d8
... ...
      "IPAddress": "172.17.0.2",
... ...

# 在容器内执行命令
[root@docker ~]# docker exec -it 9cae0af944d8 ls /
bin   dev   etc   home  proc  root  sys   tmp   usr   var
[root@docker ~]# docker exec -it 9cae0af944d8 sh
/ # 

# 拷贝文件
[root@docker ~]# docker cp 08e58d3b38bd:/etc/passwd ./
[root@docker ~]# ls
passwd

# 查看容器日志
[root@docker ~]# docker logs a420c762000f
AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message
... ...

# 删除容器
[root@docker ~]# docker rm -f de46e6254efd
# 删除所有容器
[root@docker ~]# docker rm -f $(docker ps -aq)

# 查看服务器与客户端版本
[root@docker ~]# docker version
Client: Docker Engine - Community
 Version:           20.10.10

Server: Docker Engine - Community
 Engine:
  Version:          20.10.10

# 查看配置信息
[root@docker ~]# docker info
 ... ...
 Registry Mirrors:
  https://hub-mirror.c.163.com/
 Live Restore Enabled: false
```

## 服务编排与治理

### 发布容器服务

```shell
# 通过映射宿主机端口发布服务
[root@docker ~]# docker run -itd --name myweb -p 80:80 nginx:latest 
acaeb0bd6cc214b6ddcac9ffa933f31ce15abf5b0c7d090da8ec76baea9f0978
```

### 存储卷

```shell
# 查看配置文件，创建网页目录
[root@docker ~]# docker exec -it myweb bash
[root@docker ~]# docker rm -f myweb
[root@docker ~]# mkdir /var/html

# 配置前端 nginx 后端 tomcat
[root@docker ~]# vim myweb.conf
server {
    listen       80;
    server_name  www.myweb.com;
    location / {
        root   /var/html;
        index  index.html index.htm;
    }   
    location ~ \.jsp$ {
        root           html;
        proxy_pass     http://127.0.0.1:8080;
        proxy_set_header Host $host;
    }
}

# 启动容器项目
[root@docker ~]# docker rm -f $(docker ps -aq)
[root@docker ~]# docker run -itd --name myweb -p 80:80 -v myweb.conf:/etc/nginx/conf.d/myweb.conf -v /var/html:/var/html nginx:latest
[root@docker ~]# docker run -itd --rm -v /var/html:/usr/local/tomcat/webapps/ROOT --network="container:myweb" tomcat:latest

# 添加测试页面
[root@docker ~]# echo ok >/var/html/index.html
[root@docker ~]# echo '<%=new java.util.Date()%>' >/var/html/t.jsp

# 访问验证
[root@docker ~]# curl -H "Host: www.myweb.com" http://127.0.0.1
ok
[root@docker ~]# curl -H "Host: www.myweb.com" http://127.0.0.1/t.jsp
Sun May 28 08:44:44 UTC 2023
```

### 容器服务治理

| **指令**           | **说明**                   |
| ------------------ | -------------------------- |
| up                 | 创建项目并启动容器         |
| down               | 删除项目容器及网络         |
| ls                 | 列出可以管理的项目         |
| start/stop/restart | 启动项目/停止项目/重启项目 |
| images             | 列出项目使用的镜像         |
| ps                 | 显示项目中容器的状态       |
| logs               | 查看下项目中容器的日志     |

### 项目管理

- 拷贝 docker-compose 到 /usr/bin/ 目录下

```shell
# 创建目录
[root@docker ~]# mkdir myweb
# 定义项目文件
[root@docker ~]# vim myweb/docker-compose.yaml
name: websvc
version: "3"
services:
  websvc:
    container_name: nginx
    image: nginx:latest
```

- 容器项目管理

```shell
# 创建项目，并启动
[root@docker ~]# docker-compose -f web/docker-compose.yaml up -d
[+] Running 2/2
 ⠿ Network web_default  Created      0.0s
 ⠿ Container nginx      Started      0.3s

# 查看项目
[root@docker ~]# docker-compose ls
NAME           STATUS          CONFIG FILES
web            running(1)      /root/web/docker-compose.yaml

# 查看项目中的容器状态
[root@docker ~]# docker-compose -p web ps
NAME      COMMAND                 SERVICE    STATUS      PORTS
nginx     "nginx -g 'daemon of…"  websvc     running     80/tcp

# 启动、停止、重启项目
[root@docker ~]# docker-compose -p web stop
[+] Running 1/1
 ⠿ Container nginx  Stopped         0.1s
[root@docker ~]# docker-compose -p web start
[+] Running 1/1
 ⠿ Container nginx  Started         0.2s
[root@docker ~]# docker-compose -p web restart
[+] Running 1/1
 ⠿ Container nginx  Started         0.3s
 
# 删除项目
[root@docker web]# docker-compose -p web down
[+] Running 2/2
 ⠿ Container nginx      Removed         0.1s
 ⠿ Network web_default  Removed         0.0s
```

### compose语法

| **指令**       | **说明**                                 |
| :------------- | ---------------------------------------- |
| networks       | 配置容器连接的网络                       |
| container_name | 指定容器名称                             |
| depends_on     | 解决容器的依赖、启动先后的问题           |
| command        | 覆盖容器启动后默认执行的命令             |
| environment    | 设置环境变量                             |
| image          | 指定为镜像名称或镜像  ID                 |
| network_mode   | 设置网络模式                             |
| restart        | 容器保护策略[always、no、on-failure]     |
| ports          | 暴露端口信息                             |
| volumes        | 数据卷,支持 [volume、bind、tmpfs、npipe] |

### 容器服务编排

```shell
[root@docker ~]# vim web/docker-compose.yaml 
name: myweb
version: "3"
services:
  nginx:
    container_name: nginx
    image: nginx:latest
    restart: always
    volumes:
      - type: bind
        source: myweb.conf
        target: /etc/nginx/conf.d/myweb.conf
      - type: bind
        source: /var/html
        target: /var/html
    network_mode: bridge
    ports:
      - 80:80
    environment:
      - "TZ=Asia/Shanghai"
  tomcat:
    container_name: tomcat
    image: tomcat:latest
    restart: always
    volumes:
      - type: bind
        source: /var/html
        target: /usr/local/tomcat/webapps/ROOT
    depends_on:
      - nginx
    network_mode: "container:nginx"
```

- 验证项目

```shell
# 创建,并启动项目
[root@docker ~]# docker-compose -f myweb/docker-compose.yaml up -d
[+] Running 2/2
 ⠿ Container nginx   Started                   0.9s
 ⠿ Container tomcat  Started                   0.9s
 # 查看项目
[root@docker ~]# docker-compose ls
NAME                STATUS              CONFIG FILES
myweb               running(2)          /root/myweb/docker-compose.yaml

# 查看容器状态，验证服务
[root@docker ~]# docker-compose -p myweb ps 
NAME                COMMAND                  SERVICE      STATUS
nginx               "/docker-entrypoint.…"   nginx        running
tomcat              "catalina.sh run"        tomcat       running

# 访问 php 页面验证
[root@docker ~]# curl -H "Host: www.myweb.com" http://127.0.0.1
ok
[root@docker ~]# curl -H "Host: www.myweb.com" http://127.0.0.1/t.jsp
Sun May 28 08:44:44 UTC 2023
```

