## web服务器配置管理

### nginx 安装

```shell
# 安装常用工具软件包
[root@localhost ~]# yum -y install vim bash-completion net-tools psmisc tree

# 安装编译工具和依赖包
[root@localhost ~]# yum -y install gcc make pcre-devel openssl-devel

# 下载 nginx
[root@localhost ~]# curl -LO http://nginx.org/download/nginx-1.24.0.tar.gz

# 编译安装
[root@localhost ~]# tar zxf nginx-1.24.0.tar.gz
[root@localhost ~]# cd nginx-1.24.0/
[root@localhost nginx-1.24.0]# ./configure --prefix=/usr/local/nginx --with-http_ssl_module --with-pcre
[root@localhost nginx-1.24.0]# make
[root@localhost nginx-1.24.0]# make install
[root@localhost nginx-1.24.0]# echo "hello world" >/usr/local/nginx/html/index.html
```

nginx 目录及命令说明

```shell
[root@localhost ~]# tree -d /usr/local/nginx
/usr/local/nginx  # 安装路径
├── conf          # 配置文件目录
├── html          # 网页目录
├── logs          # 日志目录
└── sbin          # 程序文件目录

[root@localhost ~]# /usr/local/nginx/sbin/nginx            #启动服务
[root@localhost ~]# /usr/local/nginx/sbin/nginx -s stop    #关闭服务
[root@localhost ~]# /usr/local/nginx/sbin/nginx -s reload  #重新加载配置文件
[root@localhost ~]# /usr/local/nginx/sbin/nginx -V         #查看软件信息
```

### 虚拟主机

- www.ok.com,    www.hello.com

```shell
# 创建 ok 域名访问目录
[root@localhost ~]# mkdir /usr/local/nginx/html/ok
[root@localhost ~]# echo "ok" >/usr/local/nginx/html/ok/index.html

# 创建 hello 域名访问目录
[root@localhost ~]# mkdir /usr/local/nginx/html/hello
[root@localhost ~]# echo "hello" >/usr/local/nginx/html/hello/index.html

# 修改配置文件，增加虚拟主机
[root@localhost ~]# vim /usr/local/nginx/conf/nginx.conf
http {
    ... ...
    server {
        listen       80;  							#端口
        server_name  www.ok.com;					#新虚拟主机定义域名
        location / {
            root   /usr/local/nginx/html/ok;		#指定网站根路径
            index  index.html index.htm;			#默认页面
        }
    }

    server {
        listen       80;  							#端口
        server_name  www.hello.com;					#新虚拟主机定义域名
        location / {
            root   /usr/local/nginx/html/hello;		#指定网站根路径
            index  index.html index.htm;			#默认页面
        }
    }
}

# 验证配置文件语法
[root@localhost ~]# /usr/local/nginx/sbin/nginx -t
nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful

[root@localhost ~]# /usr/local/nginx/sbin/nginx -s reload
```

客户端访问验证

```shell
[root@localhost ~]# curl -H "Host: www.ok.com" http://192.168.1.100
ok

[root@localhost ~]# curl -H "Host: www.hello.com" http://192.168.1.100
hello
```

### 加密网站

```shell
# 创建证书
[root@localhost ~]# cd /usr/local/nginx/conf
[root@localhost conf]# openssl genrsa -out web.key # 生成私钥
[root@localhost conf]# openssl req -x509 -key web.key -out web.pem    #生成证书,生成过程会询问诸如你在哪个国家之类的问题，可以随意回答
Country Name (2 letter code) [XX]:        # 国家名
State or Province Name (full name) []:    # 省份
Locality Name (eg, city) [Default City]:  # 城市
Organization Name (eg, company) [Default Company Ltd]:  # 公司
Organizational Unit Name (eg, section) []:  # 部门
Common Name (eg, your name or your server's hostname) []:  # 服务器名称
Email Address []:  # 电子邮件

[root@localhost conf]# ls web.*
web.key  web.pem
```

配置 https 网站

```shell
[root@localhost conf]# vim /usr/local/nginx/conf/nginx.conf
 ... ...
 server {
        listen       443 ssl;
        server_name  www.ok.com;
        ssl_certificate      web.pem;			#这里是证书文件
        ssl_certificate_key  web.key;			#这里是私钥文件

        ssl_session_cache    shared:SSL:1m;
        ssl_session_timeout  5m;

        ssl_ciphers  HIGH:!aNULL:!MD5;
        ssl_prefer_server_ciphers  on;
        
        location / {
            root   /usr/local/nginx/html/ok;		#指定网站根路径
            index  index.html index.htm;			#默认页面
        }
    }

[root@localhost conf]# /usr/local/nginx/sbin/nginx -t
nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful
[root@localhost conf]# /usr/local/nginx/sbin/nginx -s reload
```

网站验证测试

```shell
[root@localhost conf]# curl -k -H "Host: www.ok.com" https://192.168.1.100
ok
```

## 动态网站

### php-fastcgi 后端

```shell
# 安装 php 后端
[root@localhost ~]# yum install -y php-fpm
[root@localhost ~]# systemctl enable php-fpm.service
Created symlink from /etc/systemd/system/multi-user.target.wants/php-fpm.service to /usr/lib/systemd/system/php-fpm.service.
[root@localhost ~]# systemctl start php-fpm.service

# 分离解析 php 
[root@localhost ～]# vim /usr/local/nginx/conf/nginx.conf
 server {
 ... ...
        location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            include        fastcgi.conf;
        }

[root@localhost conf]# /usr/local/nginx/sbin/nginx -t
nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful
[root@localhost conf]# /usr/local/nginx/sbin/nginx -s reload

# 添加 php 测试页面
[root@localhost html]# cat >/usr/local/nginx/html/t.php <<EOF
<?PHP
  print("is php")."\n";
?>
EOF
```

验证测试

```shell
[root@localhost html]# curl http://192.168.1.100/t.php
is php
```

### tomcat 后端

```shell
# 安装 tomcat
[root@localhost ~]# yum install -y java-11-openjdk
[root@localhost ~]# curl -LO https://dlcdn.apache.org/tomcat/tomcat-10/v10.1.9/bin/apache-tomcat-10.1.9.tar.gz
[root@localhost ~]# tar zxf apache-tomcat-10.1.9.tar.gz
[root@localhost ~]# mv apache-tomcat-10.1.9 /usr/local/tomcat
[root@localhost ~]# vim /usr/local/tomcat/conf/server.xml 
      <Host name="localhost"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">
            <Context docBase="/usr/local/nginx/html/" path="/"/>   # 新添加
[root@localhost ~]# vim /usr/local/nginx/conf/nginx.conf
   ... ...

        location ~ \.jsp$ {
            root           html;
            proxy_pass     http://127.0.0.1:8080;
            proxy_set_header Host $host;
        }
# 添加 jsp 测试页面
[root@localhost html]# cat >/usr/local/nginx/html/t.jsp <<EOF
<%=new java.util.Date()%>
EOF
```

验证测试

```shell
[root@localhost ~]# curl http://127.0.0.1/t.jsp
Sat May 27 10:59:03 CST 2023
```

## 负载均衡

图例

```mermaid
flowchart LR
c[(client)] ---> R[(route<br>192.168.1.100)] ===> w1[(web1<br>192.168.2.11)] & w2[(web2<br>192.168.2.12)]
```

安装部署

```shell
# 打包 nginx
[root@localhost ~]# tar -czf nginx.tar.gz -C /usr/local nginx
[root@localhost ~]# scp nginx.tar.gz 192.168.2.11:./
[root@localhost ~]# scp nginx.tar.gz 192.168.2.12:./
#---------------------------------------------------------#
[root@web1 ~]# yum install -y openssl pcre
[root@web1 ~]# tar zxf nginx.tar.gz -C /usr/local/
[root@web1 ~]# echo 'is web1' >/usr/local/nginx/html/index.html
[root@web1 ~]# /usr/local/nginx/sbin/nginx
#---------------------------------------------------------#
[root@web2 ~]# yum install -y openssl pcre
[root@web2 ~]# tar zxf nginx.tar.gz -C /usr/local/
[root@web2 ~]# echo 'is web2' >/usr/local/nginx/html/index.html
[root@web2 ~]# /usr/local/nginx/sbin/nginx
```

配置负责均衡

```shell
[root@localhost ~]# vim /usr/local/nginx/conf/nginx.conf
http {
    upstream myapp {
        server 192.168.2.11;
        server 192.168.2.12;
    }
    ...
}
    ...
    server
        location / {
            proxy_pass http://myapp;
            proxy_set_header Host $host;
  
        }
```

验证测试

```shell
[root@t1 ~]# curl http://127.0.0.1
is web1
[root@t1 ~]# curl http://127.0.0.1
is web2
```

