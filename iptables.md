## Linux 系统管理

### iptables

`iptables -t 表 -[AIDLvn] 链 [条件匹配] -m [模块] -j [处理方法]`

| 参数 | 说明                                          |
| ---- | --------------------------------------------- |
| A    | 添加一个规则                                  |
| I    | 在指定位置插入一个规则                        |
| D    | 删除一个规则                                  |
| F    | 清空指定链中的所有规则                        |
| L    | 列出所有规则                                  |
| v    | 显示详细信息，如数据包的数量和大小等          |
| n    | 不进行 DNS 解析，直接使用 IP 地址作为源或目标 |

表和链

| 表       | 描述                                                         |
| -------- | ------------------------------------------------------------ |
| filter   | 包过滤模块，用于过滤数据包。                                 |
| nat      | 用于网络地址转换（NAT），可以修改数据包的目的地址和来源地址等信息。 |
| mangle   | 允许修改数据包的各种标记，比如TOS/DSCP字段、TTL字段等。      |
| raw      | 用于连接跟踪之前的数据处理。                                 |
| security | 与 SELinux 一起使用，实现不同等级的安全策略。                |

链：

| 链          | 描述                                                         |
| ----------- | ------------------------------------------------------------ |
| INPUT       | 处理目标是本机的数据包，本地回环接口和内核生成的数据包。     |
| OUTPUT      | 处理来源于本机的数据包，包括本地回环接口、内核生成的数据包。 |
| FORWARD     | 处理通过流经本机的转发数据包。                               |
| PREROUTING  | 处理到达系统的网络数据包，可以进行地址转换等修改操作。       |
| POSTROUTING | 处理离开系统的网络数据包，可以进行 NAT 转换等修改操作。      |

表和链关系图：

| 表名   | 描述                   | 链名        | 描述                                                 |
| ------ | ---------------------- | ----------- | ---------------------------------------------------- |
| filter | 控制进出本机的数据包   | INPUT       | 处理目标是本机的数据包                               |
|        |                        | FORWARD     | 转发的数据包                                         |
|        |                        | OUTPUT      | 处理来源于本机的数据包                               |
| nat    | 使 IP 地址和端口号转换 | PREROUTING  | 在数据包进入路由之前修改目的地址                     |
|        |                        | INPUT       | 在数据包进入本地计算机之前修改目的地址               |
|        |                        | OUTPUT      | 在数据包离开本地计算机并发送到外部网络之前修改源地址 |
|        |                        | POSTROUTING | 在数据包离开本地计算机之后修改源地址                 |
| raw    | 修改原始数据包         | PREROUTING  | 在数据包进入路由之前进行修改                         |
|        |                        | OUTPUT      | 在数据包离开本地计算机之前进行修改                   |
| mangle | 修改数据包头部的信息   | PREROUTING  | 在数据包进入路由之前修改数据包                       |
|        |                        | INPUT       | 处理目标是本机的数据包                               |
|        |                        | FORWARD     | 转发的数据包                                         |
|        |                        | OUTPUT      | 处理来源于本机的数据包                               |
|        |                        | POSTROUTING | 在数据包离开系统之前修改数据包                       |

选项与参数

| 选项 | 参数             | 描述                               |
| ---- | ---------------- | ---------------------------------- |
| `-s` | `address[/mask]` | 指定来源IP地址或网段               |
| `-d` | `address[/mask]` | 指定目标IP地址或网段               |
| `-p` | `protocol`       | 指定传输层协议（TCP、UDP、ICMP等） |
| `-i` | `interface`      | 指定进入网络接口                   |
| `-o` | `interface`      | 指定出去网络接口                   |
| `-m` | `module`         | 加载扩展模块                       |
| `-j` | `target`         | 指定规则的动作（ACCEPT、DROP等）   |

### 数据包流程图

```mermaid
graph LR
    data1((数据包)) --> L4([PREROUTING]) --> R0{路由}
    R0 --> L1([INPUT]) --> P[(程序处理)] --> L2([OUTPUT]) --> L5([POSTROUTING])
    R0 ---> L3([FORWARD]) ---> L5 --> data2((Internet))
```

## 主机型防火墙

```mermaid
flowchart LR
  subgraph Linux主机
    VM8(Linux<br>主<br>机<br>防<br>火<br>墙) o--o VM3([web<br>应用服务]) & VM4([数据库<br>应用服务])
  end
  VM1[(Linux<br>客户机)] o--o NET8((VMnet8<br>虚拟网卡)) o--o VM8   
```

### 实验案例

```shell
# 禁止 client 使用 ping 探测 server
[root@server ~]# iptables -t filter -A INPUT -s 192.168.1.11 -d 192.168.1.10 -p icmp -j DROP

# 禁止所有主机使用 ping 探测， client 除外
[root@server ~]# iptables -t filter -F INPUT 
[root@server ~]# iptables -t filter -A INPUT -s 192.168.1.11 -d 192.168.1.10 -p icmp -j ACCEPT
[root@server ~]# iptables -t filter -A INPUT -d 192.168.1.10 -p icmp -j DROP

# 每分钟只允许 ping 5 次，使用 limit 模块限制频率
[root@server ~]# iptables -t filter -F INPUT 
[root@server ~]# iptables -t filter -A INPUT -s 192.168.1.11 -d 192.168.1.10 -p icmp -m limit --limit 5/minute -j ACCEPT
[root@server ~]# iptables -t filter -A INPUT -d 192.168.1.10 -p icmp -j DROP

# 每分钟只允许访问 web 5 次，由于 tcp/ip 有三次握手，四次断开，属于有状态连接
# 使用 state 模块匹配 tcp/ip 的连接状态
[root@server ~]# iptables -t filter -F INPUT
# 允许已经建立连接的应用通过
[root@server ~]# iptables -t filter -A INPUT -p tcp -m state --state ESTABLISHED,RELATED -j ACCEPT
# 对新建连接的应用限制频率
[root@server ~]# iptables -t filter -A INPUT -d 192.168.1.10 -p tcp --dport 80 -m limit --limit 5/minute -m state --state NEW -j ACCEPT
[root@server ~]# iptables -t filter -A INPUT -d 192.168.1.10 -p tcp --dport 80 -j DROP
```

## 网络型防火墙

```mermaid
flowchart LR
  VM1[(Linux<br>客户机)] o--o NET8((VMnet8<br>虚拟网卡)) o--o VM2
  subgraph 内网
    VM2(Linux<br>网<br>络<br>防<br>火<br>墙) o--o NET1((VMnet1<br>虚拟网卡)) o--o VM3[(web<br>服务器)] & VM4[(数据库<br>服务器)]
  end
 
```

### 基础环境准备

#### 图例

```mermaid
flowchart LR
L1[(client<br>192.168.1.20)] --- n1{{vmnet8<br>192.168.1.0/24}} --- L2[(route)] --- n2{{vmnet1<br>192.168.2.0/24}} --- L3[(web1<br>192.168.2.11)] & L4[(web2<br>192.168.2.12)]
```



## client 主机

```sh
[root@localhost ~]# hostnamectl set-hostname client
[root@localhost ~]# cat >/etc/sysconfig/network-scripts/ifcfg-eth0<<EOF
DEVICE="eth0"
ONBOOT="yes"
NM_CONTROLLED="yes"
TYPE="Ethernet"
BOOTPROTO="static"
IPADDR="192.168.1.20"
PREFIX=24
GATEWAY="192.168.1.100"
EOF
[root@localhost ~]# reboot
```

#### route 主机

```shell
[root@localhost ~]# hostnamectl set-hostname route
[root@localhost ~]# cat >/etc/sysconfig/network-scripts/ifcfg-eth0<<EOF
DEVICE="eth0"
ONBOOT="yes"
NM_CONTROLLED="yes"
TYPE="Ethernet"
BOOTPROTO="static"
IPADDR="192.168.1.100"
PREFIX=24
GATEWAY="192.168.1.254"
EOF
[root@localhost ~]# cat >/etc/sysconfig/network-scripts/ifcfg-eth1<<EOF
DEVICE="eth1"
ONBOOT="yes"
NM_CONTROLLED="yes"
TYPE="Ethernet"
BOOTPROTO="static"
IPADDR="192.168.2.100"
PREFIX=24
EOF
[root@localhost ~]# reboot
```

#### web1主机

```shell
[root@localhost ~]# hostnamectl set-hostname web1
[root@localhost ~]# cat >/etc/sysconfig/network-scripts/ifcfg-eth0<<EOF
DEVICE="eth0"
ONBOOT="yes"
NM_CONTROLLED="yes"
TYPE="Ethernet"
BOOTPROTO="static"
IPADDR="192.168.2.11"
PREFIX=24
GATEWAY="192.168.2.100"
EOF
[root@localhost ~]# reboot
```

#### web2 主机

```shell
[root@localhost ~]# hostnamectl set-hostname web2
[root@localhost ~]# cat >/etc/sysconfig/network-scripts/ifcfg-eth0<<EOF
DEVICE="eth0"
ONBOOT="yes"
NM_CONTROLLED="yes"
TYPE="Ethernet"
BOOTPROTO="static"
IPADDR="192.168.2.12"
PREFIX=24
GATEWAY="192.168.2.100"
EOF
[root@localhost ~]# reboot
```

### 实验案例

```shell
# 启用路由转发
[root@route ~]# sysctl -w net.ipv4.ip_forward=1

# 禁止 ping 探测 server
[root@route ~]# iptables -t filter -A FORWARD -p icmp -j DROP

# 每分钟只允许 ping 5 次，使用 limit 模块限制频率 
[root@route ~]# iptables -t filter -I FORWARD 1 -p icmp -m limit --limit 10/minute --limit-burst 10 -j ACCEPT

# 每分钟尝试 3 次 ssh 登录，防止暴力破解密码
# 对已经连接的用户放行
[root@route ~]# iptables -t filter -A FORWARD -p tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
# 限制频率
[root@route ~]# iptables -t filter -A FORWARD -p tcp -m tcp --dport 22 -m state --state NEW -m limit --limit 3/min -j ACCEPT
# 拒绝超过频率的请求
[root@route ~]# iptables -t filter -A FORWARD -p tcp -m tcp --dport 22 -j DROP


# 源地址伪装
[root@route ~]# iptables -t nat -A POSTROUTING -s 192.168.2.11 -j MASQUERADE

# 目标地址重定向，把 1122 定向到 192.168.2.11 的 22 端口
[root@route ~]# iptables -t nat -A PREROUTING -p tcp --dport 1122 -j DNAT --to 192.168.2.11:22

# 目标地址重定向，把 1222 定向到 192.168.2.12 的 22 端口
[root@route ~]# iptables -t nat -A PREROUTING -p tcp --dport 1222 -j DNAT --to 192.168.2.12:22
```

