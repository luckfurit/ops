## Linux常见面试题

### 简述Linux常见目录及其作用？

```shell
/
├── bin
├── sbin
├── etc
├── home
├── root
├── tmp
├── usr
└── var
```

### **linux 如何挂载网络文件系统**

```javascript
yum install -y nfs-utils
mount -t nfs 192.168.1.20:/var/data /mnt/
```

### 查看实时 http 的并发请求数与其 TCP 连接状态**

```javascript
netstat -ant |awk '$1=="tcp"{print $6}' |sort |uniq -c
ss -ant|awk '{if($1~/^[A-Z]+$/)A[$1]++}END{for(a in A)print A[a],a}'
```

### **统计/var/log 目录下文件数量**

```javascript
ls -lR /var/log/|grep ^-|wc -l
find /var/log -type f|grep -c .
```

### **命令的执行结果是什么 **

```javascript
echo 123 |sed -r 's2([123])2\112g'   # 112131
```

### **生成随机8位密码**

```javascript
tr -dc '[:alnum:]' </dev/urandom |head -c 8
```

### **/etc/passwd 各字段的含义**

```javascript
root:x:0:0:root:/root:/bin/bash
```

### **9.ps aux 中的 VSZ 代表什么意思，RSS 代表什么意思**

```shell
VSZ 虚拟内存，  RSS 实际占用内存
```

### **软链接与硬链接的区别**

```shell
```

### **在 1-39 内取随机数**

```shell
$((RANDOM%39+1))
```

### 去除 #注释的行 和 空行

```shell
# yaml file
metadata:
  name: myweb
  # labels:
  
spec: 
  containers:
  - name: linux
    image: linux  
# grep -Pv "^\s*(#|$)" urfile
```

### 使用 grub 提取IP地址

```javascript
1.2.3.4.5
11.222.333.44
255.256.254.253
192.168.5
249.231.222.210
0.0.0.1

# grep -Pv "^((25[0-5]|2[0-4]\d|1?\d?\d)\.){3}(25[0-5]|2[0-4]\d|1?\d?\d)$" urfile
```

### **判断 192.168.1.0/24 网络里，当前在线的 IP 有哪些，能 ping 通则认为在线**

```shell
nmap -sn 192.168.1.1-255
```

### 文件被删除了，空间为什么没有被释放

```shell
有程序在使用，需要程序关闭或重启对应的服务
```

### 简述TCP与UDP的区别，以及它们的通讯方式

```shell
udp 无连接，直接发送数据包，不保证安全性
tcp 有连接，需要三次握手后才能发送数据包，有数据安全性校验，断开也需要相互发送数据包及确认信息
```

