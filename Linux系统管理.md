# 软件运维

## Unix/Linux 简介

```html
    Unix 的故事可以追溯到1960年代末期，在当时计算机硬件技术的发展已经使得操作系统成为了一个必需的组成部分。
    当时的操作系统主要基于批处理模式，用户需要将程序提交给操作员，再由操作员按照一定顺序执行。这种方式效率低下，不利于交互式使用。
    因此，在贝尔实验室工作的 Ken Thompson 和 Dennis Ritchie 开始着手开发一款新的，能够支持交互式的操作系统。

    他最初采用汇编语言编写了一个简单的操作系统内核 Unics，后来改名为 Unix。在早期的计算机领域，程序通常是用汇编语言编写的，这种语言很难阅读和维护。
    为了提高程序员的工作效率，Dennis Ritchie 开始着手开发一种新的高级编程语言，并将其命名为 C 语言。
    C 语言采用了类似于汇编语言的语法，但是具有更高的抽象度和可读性，能够快速开发高质量的软件。
    同时，C 语言还具有跨平台的优势，可以在不同的硬件和操作系统上运行，这使得它成为了 Unix 系统开发的理想语言。

    Unix 的成功有几个重要原因。首先，它是一个模块化、可扩展的操作系统，可以很方便地添加新的功能或者定制特定应用场景的版本。
    其次，Unix 采用了类似文件的抽象概念，允许用户通过文件系统进行各种操作，这种设计思想被广泛地应用于其他操作系统中。
    最后，Unix 可以在各种硬件平台上运行，这使得它成为了跨平台开发的理想选择。

    Linux 诞生的故事始于1991年，在这一年，芬兰赫尔辛基大学的计算机科学系学生 Linus Torvalds 开始开发一个操作系统内核。
    当时他希望能够在自己的个人电脑上运行类 Unix 操作系统，但是市场上的类 Unix 系统价格昂贵，而且对硬件配置有严格限制，因此他决定自己动手写一个。
    Linus 将这个项目命名为 "Linux"，这个名称结合了他的名字和 "Unix" 操作系统。
    Linux 内核在不久之后就成为了开源软件，这意味着任何人都可以自由地查看、使用和修改它的源代码。
    这种开放性原则吸引了更多的程序员参与到 Linux 的开发中来，使得它不断地发展壮大。

    现在，Linux 已经成为世界上最流行的服务器操作系统，同时也被越来越多的个人用户所采用。
    它的优秀的安全性、可靠性、稳定性以及灵活性等特点，让它成为了许多企业和组织的首选操作系统。
```

## Linux 的发行版

```html
    尽管 Linux 内核是操作系统中最核心的部分，但只有内核还不足以构建一个完整的操作系统。
    一个完整的操作系统除了内核所提供的功能之外，还需要系统配置和管理工具以及形形色色的工具及软件。
    例如：如系统监视器、日志查看器、文本编辑工具、编译器、调试器、绘图工具等。

    不同的厂商，使用 Linux 的内核和 GNU 提供的工具软件构建了各自的 Linux 操作系统，这些系统就是不同的 Linux 发行版。
    例如：Ubuntu、Debian、Fedora、CentOS、RedHat 等。在总多的发行版中，CentOS 以稳定性著称，被企业广泛使用。
```

## Linux 管理命令

### 文件目录管理

| 命令  | 描述                                   | 备注                                           |
| ----- | -------------------------------------- | ---------------------------------------------- |
| ls    | 列出目录中的文件和子目录               | 常用选项：`-l` 显示详细信息；`-a` 显示所有文件 |
| cd    | 改变当前工作目录                       | `.` 当前目录，`..` 上一级目录，`~` 用户家目录  |
| pwd   | 显示当前工作目录的路径名               | `-P` 真实路径                                  |
| mkdir | 创建一个新目录                         | 常用选项：`-p` 创建多层嵌套目录                |
| rmdir | 删除一个空目录                         | 只能删除空目录，否则需要使用 `rm` 命令         |
| touch | 创建一个空文件或者修改已有文件的时间戳 | 如果文件不存在会先创建一个空文件               |
| rm    | 删除文件或目录                         | `-r` 递归删除；`-f` 强制删除，不提示确认       |
| cp    | 复制文件或目录                         | `-a` 保留属性递归复制目录及其内容              |
| mv    | 移动或重命名文件或目录                 | 无                                             |
| ln    | 创建链接                               | 常用选项：`-s` 可以创建符号链接                |

#### 命令案例（一）

```shell
# 创建名为 test 的目录
[root@localhost ~]# mkdir test
[root@localhost ~]# ls
test

# 在 test 目录下创建名为 file1 和 file2 的空文件
[root@localhost ~]# cd test
[root@localhost test]# touch file1 file2
[root@localhost test]# ls
file1  file2

# 在 test 目录下创建名为 subtest 的子目录
[root@localhost test]# mkdir subtest
[root@localhost test]# ls
file1  file2  subtest

# 切换到 subtest 目录下，并创建一个新文件
[root@localhost test]# cd subtest
[root@localhost subtest]# touch newfile.txt
[root@localhost subtest]# ls
newfile.txt

# 返回上级目录，并列出该目录下的所有内容，包括子目录和文件
[root@localhost subtest]# cd ..
[root@localhost test]# ls -al
total 12
drwxr-xr-x. 3 root root 4096 Oct 13 17:41 .
dr-xr-x---. 5 root root 4096 Oct 13 17:41 ..
drwxr-xr-x. 2 root root 4096 Oct 13 17:42 subtest
-rw-r--r--. 1 root root    0 Oct 13 17:41 file1
-rw-r--r--. 1 root root    0 Oct 13 17:41 file2

# 复制 test 目录及其子目录到当前工作目录下的 backup 目录，并检查是否复制成功
[root@localhost test]# cp -r test ~/backup
[root@localhost test]# ls ~/backup/test
file1  file2  subtest

# 重命名备份目录下的 test 文件夹为 test_backup，并检查是否重命名成功
[root@localhost test]# mv ~/backup/test ~/backup/test_backup
[root@localhost test]# ls ~/backup
test_backup

# 创建 test 目录下的 file1 的符号链接，并检查是否创建成功
[root@localhost test]# ln -s file1 file1_link
[root@localhost test]# ls -al

# 删除 subtest 目录及其所有内容，并检查是否删除成功
[root@localhost test]# rm -r subtest
[root@localhost test]# ls

# 删除符号链接和文件，并检查是否删除成功
[root@localhost test]# rm file1_link file1 file2
[root@localhost test]# ls
```

### 帮助及手册

| 命令    | 描述                         | 备注                               |
| ------- | ---------------------------- | ---------------------------------- |
| man     | 显示命令的手册页             | 手册页包含命令的详细说明和用法     |
| help    | 显示 Bash 内置命令的帮助信息 | 只能显示 Bash 内置命令的帮助信息   |
| info    | 显示命令的信息               | 与 man 类似，但提供更详细的信息    |
| whatis  | 显示命令的简短描述           | 只显示命令的简短描述               |
| apropos | 查找与关键字相关的命令       | 可以用来查找不知道具体命令名的命令 |

#### 命令案例（二）

```shell
# 显示 man 命令的手册页
[root@localhost ~]# man ls
LS(1)              User Commands                LS(1)             NAME
       ls - list directory contents
...

# 显示 Bash 内置命令的帮助信息
[root@localhost ~]# help cd
cd: cd [-L|[-P [-e]] [-@]] [dir]
    Change the shell working directory.
...

# 显示命令的信息
[root@localhost ~]# info tar
tar (GNU tar) 1.26
...

# 显示命令的简短描述
[root@localhost ~]# whatis grep
grep (1)            - print lines matching a pattern

# 查找与关键字相关的命令
[root@localhost ~]# apropos network
ifconfig (8)         - configure a network interface
ip (8)               - show / manipulate routing, devices, policy routing and tunnels
ip-address (8)       - protocol address management
...
```

### 文件管理命令

| 命令 | 描述                     | 备注                                    |
| ---- | ------------------------ | --------------------------------------- |
| cat  | 连接文件并打印到标准输出 | 可以用来查看文件内容                    |
| tac  | 将文件逆序打印到标准输出 | 与 cat 相反，可以用来查看文件末尾的内容 |
| tail | 显示文件末尾的内容       | -f 监控正在写入的日志文件               |
| head | 显示文件开头的内容       | 可以用来查看文件的前几行                |
| less | 分页显示文件内容         | 交互式操作，只读                        |
| vim  | 编辑文本文件             | 交互式操作，读写                        |

#### 命令案例（三）

```shell
# 显示文件内容
[root@localhost ~]# cat /etc/resolv.conf 
; generated by /usr/sbin/dhclient-script
nameserver 192.168.1.254
search localhost 

# 倒序显示文件内容
[root@localhost ~]# tac /etc/resolv.conf 
search localhost
nameserver 192.168.1.254
; generated by /usr/sbin/dhclient-script

# 查看文件开头的内容
[root@localhost ~]# head /etc/passwd 
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin 
... ...

# 查看文件末尾的内容
[root@localhost ~]# tail /etc/passwd 
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
... ...

# 只读查看文件，可交互操作
[root@localhost ~]# less /var/log/messages

# 读写文件
[root@localhost ~]# vim /etc/hosts
```

### 文件查找

| 命令     | 描述                                 | 备注                                     |
| -------- | ------------------------------------ | ---------------------------------------- |
| find     | 在指定目录下查找文件                 | 支持多种条件，例如：文件名、类型、大小等 |
| updatedb | 更新 locate 命令使用的数据库         | locate 查询该数据库，并不是实时数据      |
| locate   | 快速定位文件                         | 使用 updatedb 命令更新数据库后才能使用   |
| which    | 查找可执行文件的位置                 | 可以用于查看系统命令是否存在             |
| whereis  | 查找二进制文件、源码、帮助文档的位置 | 可以一次性查找多种类型的文件             |

#### 命令案例（四）

```shell
# 使用 find 命令查找当前目录下所有的 txt 文件
[root@localhost ~]# find /etc -name "*.txt"
/etc/pki/nssdb/pkcs11.txt

# 使用 updatedb 命令更新 locate 命令使用的数据库
[root@localhost ~]# updatedb
（无输出）

# 使用 locate 命令查找系统中所有包含关键字 "bashrc" 的文件
[root@localhost ~]# locate bashrc
/etc/bashrc
/etc/skel/.bashrc
/root/.bashrc

# 使用 which 命令查找系统中 ls 命令的位置
[root@localhost ~]# which ls
/bin/ls

# 使用 whereis 命令查找系统中 passwd 命令的位置、源代码文件和帮助文档
[root@localhost ~]# whereis passwd
passwd: /usr/bin/passwd /etc/passwd /usr/share/man/man1/passwd.1.gz
```

### Shell 操作符

| 操作符 | 描述                                                         |
| ------ | ------------------------------------------------------------ |
| `>`    | 输出重定向，将命令的输出写入一个文件或另一个命令的输入。     |
| `>>`   | 追加重定向，将命令的输出附加到文件的末尾。                   |
| `|`    | 管道操作符，将一个命令的输出作为另一个命令的输入。           |
| `&`    | 后台运行符，使一个进程在后台运行，不会阻塞终端。             |
| `;`    | 命令分隔符，允许在一行中运行多个命令。                       |
| `&&`   | 逻辑与操作符，如果第一个命令成功执行，则执行第二个命令。     |
| `||`   | 逻辑或操作符，如果第一个命令失败，则执行第二个命令。         |
| `$`    | 变量替换符，可以在命令中引用变量的值。                       |
| `*`    | 通配符，匹配任何字符。                                       |
| `?`    | 通配符，匹配任何单个字符。                                   |
| `[]`   | 通配符，匹配指定范围内的任何字符。                           |
| {x..y} | 扩展序列，从 x 开始，至  y  结束                             |
| `<`    | 输入重定向符号，将一个文件或命令的输出作为另一个命令的输入。 |
| `<<`   | Here 文档操作符号，允许用户将一段文本作为输入传递给一个命令，可以使用引号关闭扩展解析。 |
| `<<<`  | Here 字符串操作符号，类似于 `<<`，但是输入的字符串被视为单个参数而不是多行文本。 |

#### 命令案例（五）

```shell
# 将 "Hello, World!" 写入 example.txt 文件
[root@localhost ~]# echo "Hello, World!" > example.txt
[root@localhost ~]# cat example.txt
Hello, World!

# 追加内容到 example.txt 文件中
[root@localhost ~]# echo "This is an example file." >> example.txt
[root@localhost ~]# cat example.txt
Hello, World!
This is an example file.

# 使用管道操作符将 example.txt 文件内容传递给 grep 命令进行搜索，验证结果是否正确
[root@localhost ~]# cat example.txt | grep "example"
This is an example file.

# 后台运行一个长时间的命令
[root@localhost ~]# sleep 600 &
[1] 12674

# 5 秒以后执行 echo 命令
[root@localhost ~]# sleep 5; echo hello world
hello world

# 条件判断，成功执行
[root@localhost ~]# test -f example.txt && echo "file is exists"
file is exists

# 条件判断，失败执行
[root@localhost ~]# test -f example.txt || echo "file is not exists"

# 设置变量并在命令中使用它
[root@localhost ~]# echo ${PATH}
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin

# 在屏幕上输出 0 到 9 的数字
[root@localhost ~]# echo {0..9}
0 1 2 3 4 5 6 7 8 9

# 从文件 input.txt 中读取内容，并将结果写入 output.txx 文件中
[root@localhost ~]# cat < example.txt > output.txx

# 列出当前目录下所有以 .tx + 任意字符结尾的文件
[root@localhost ~]# ls *.tx?
example.txt   output.txx

# 列出以 a-g 开头的文件
[root@localhost ~]# ls [a-g]*
example.txt   output.txx

# 使用 Here 文档操作符将一段文本传递给 cat 命令，输出结果
[root@localhost ~]# cat << EOF
> This is a multi-line
> text input using Here document.
> EOF
This is a multi-line
text input using Here document.

# 使用 Here 字符串操作符将文本作为参数传递给 grep 命令进行搜索
[root@localhost ~]# grep "example" <<< "This is an example text."
This is an example text.
```

### 用户管理

Linux 的用户是存储在文本文件中的，主要涉及 4 个文件。

#### /etc/passwd

这个文件包含了当前系统上所有用户账户的基本信息，各字段的含义如下：

```
username:password:UID:GID:comment:home_directory:default_shell
```

> `username`：该用户账户的用户名；
> `password`：密码散列值，由于安全性考虑，现在通常是一个占位符`x`，实际密码保存在 `/etc/shadow` 中；
> `UID`： 用户ID（User ID），是一个数字，用于标识该用户在系统中的唯一身份。
> `GID`： 组ID（Group ID），是一个数字，用于标识该用户属于哪个用户组。
> `comment`：注释，可随意填写，但一般会包含一些关于该用户的备注信息；
> `home_directory`：该用户的家目录，即登录后默认所在的目录；
> `default_shell`：该用户登录时使用的shell，默认是Bash。

#### /etc/shadow

这个文件存储了密码加密后的散列值以及其他用户账户的安全相关信息，各字段的含义如下：

```
username:password:last_changed_days_since_1970:minimum_days_between_changes:maximum_days_between_changes:warning_days_before_expiry:account_inactive_days:max_days_of_validity
```

> `username`: 该用户账户的用户名；
> `password`: 密码散列值；
> `last_changed_days_since_1970`: 上次修改密码的日期（自1970年1月1日以来的天数）；
> `minimum_days_between_changes`: 最短修改密码间隔天数，即用户至少需要在该天数之后才能修改密码；
> `maximum_days_between_changes`: 最长修改密码间隔天数，即超过该天数后，用户必须强制修改密码；
> `warning_days_before_expiry`: 在密码过期之前多少天开始发出警告提示；
> `account_inactive_days`: 账户在多少天内没有被使用则被锁定；
> `max_days_of_validity`: 密码的最大有效天数。

#### /etc/group

这个文件记录了系统上所有用户组的基本信息，各字段的含义如下：

```
group_name:password:GID:user_list
```

> `group_name`：组名；
> `password`：组密码，通常不会填写；
> `GID`：组ID，用于标识该用户组在系统中的唯一身份；
> `user_list`：属于该用户组的用户名列表，用逗号分隔。

#### /etc/gshadow

这个文件存储每个用户组的组密码，各字段的含义如下：

```
group_name:password:user_list
```

> `group_name`：组名；
> `password`：组密码，和 `/etc/shadow` 文件中的用户密码类似；
> `user_list`：属于该用户组的用户名列表，用逗号分隔。

### 用户管理命令

| 命令     | 描述                               | 备注                      |
| -------- | ---------------------------------- | ------------------------- |
| useradd  | 用于创建新用户账户                 | 需要root权限              |
| passwd   | 用于修改用户的密码                 | 需要root权限              |
| usermod  | 用于修改用户账户的属性             | 可以修改用户名、用户ID等  |
| userdel  | 用于删除用户账户                   | 同时也会删除用户的主目录  |
| chage    | 用于修改用户的密码过期时间         | 需要root权限              |
| groupadd | 用于创建新的用户组                 | 需要root权限              |
| groupdel | 用于删除用户组                     | 只能删除空用户组          |
| groupmod | 用于修改用户组的属性               | 可以修改用户组的名称或GID |
| id       | 用于显示当前用户的 ID 和所属组     |                           |
| sudo     | 允许以其他用户的身份执行特定的命令 | 使用当前用户密码          |
| su       | 用于切换当前用户的身份             | 使用目标用户密码          |

#### 命令案例（六）

```shell
# 1. useradd - 添加新用户
[root@localhost ~]# useradd -m -c "John Doe" -s /bin/bash johndoe

# 2. 查看用户信息
[root@localhost ~]# id johndoe
uid=1001(johndoe) gid=1001(johndoe) groups=1001(johndoe)

# 3. passwd - 为用户设置密码
[root@localhost ~]# passwd johndoe
Changing password for user johndoe.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.

# 4. usermod - 修改用户属性
[root@localhost ~]# usermod -c "Johnathan Doe" -s /bin/sh johndoe

# 5. groupadd - 添加新组
[root@localhost ~]# groupadd developers

# 6. groupmod - 修改组属性
[root@localhost ~]# groupmod -n dev_team developers

# 7. groupdel - 删除组
[root@localhost ~]# groupdel dev_team

# 8. chage - 更改用户密码过期信息
[root@localhost ~]# chage -M 60 johndoe

# 查看用户密码过期时间
[root@localhost ~]# chage -l johndoe
Last password change                                    : May 09, 2023
Password expires                                        : Jul 08, 2023
Password inactive                                       : never
Account expires                                         : never
Minimum number of days between password change          : 0
Maximum number of days between password change          : 60
Number of days of warning before password expires       : 7

# 9. su - 切换用户身份
[root@localhost ~]# su - johndoe
[johndoe@localhost ~]$ id
uid=1001(johndoe) gid=1001(johndoe) groups=1001(johndoe)

# 10. sudo - 以其他用户身份执行命令（通常为root用户）
[johndoe@localhost ~]$ sudo yum update
[sudo] password for johndoe:
Loaded plugins: fastestmirror
Determining fastest mirrors
...

# 11. userdel - 删除用户
[root@localhost ~]# userdel -r johndoe
```

### 权限管理

在 Linux 中，每个文件或目录都有三种基本权限：读取（r）、写入（w）和执行（x），分别对应数字 4、2 和 1。这些权限位可以组合使用，以实现不同的访问控制策略。此外，还有一些特殊权限位，如 SUID、SGID 和 Sticky Bit，它们可以影响文件或目录的行为。

以下是各个权限位的作用：

| 权限位     | 描述                                                         |
| ---------- | ------------------------------------------------------------ |
| r          | 允许读取文件内容或查看目录中的文件列表                       |
| w          | 允许修改文件内容或在目录中创建、删除或重命名文件             |
| x          | 允许执行文件或进入目录                                       |
| SUID       | 对于可执行文件，使其在执行时具有文件所有者的权限而不是执行者的权限 |
| SGID       | 对于目录，使新创建的文件或目录继承该目录的所属组             |
| Sticky Bit | 对于目录，防止其他用户删除该目录下的文件或目录               |

例如，一个文件的权限为 `-rw-r--r--`，即所有者具有读写权限，其他用户只能读取。这意味着所有者可以读取和修改文件内容，其他用户只能读取文件内容。如果将该文件的权限修改为 `-rwsr-xr-x`，则表示该文件具有 SUID 权限，即在执行该文件时，会以文件所有者的身份来执行。同时，其他用户也可以执行该文件，但只能读取文件内容。

类似地，如果一个目录的权限为 `drwxrwsr-x`，即所有者和所属组具有读、写和执行权限，并且新创建的文件或目录都继承该目录的所属组。此外，该目录还具有 Sticky Bit 权限，这意味着其他用户无法删除该目录下的文件或目录，除非他们是文件或目录的所有者或管理员。

### 权限管理命令

| 命令    | 描述                               | 备注                               |
| ------- | ---------------------------------- | ---------------------------------- |
| chmod   | 用于修改文件或目录的权限           | 可以使用数字或符号来指定权限       |
| chown   | 用于修改文件或目录的所有者和所属组 | 需要管理员权限                     |
| getfacl | 用于获取文件或目录的访问控制列表   | 可以查看文件或目录的详细权限信息   |
| setfacl | 用于设置文件或目录的访问控制列表   | 可以为文件或目录添加额外的权限规则 |

#### 命令案例（七）

```shell
# 创建一个名为 test.txt 的文件
[root@localhost ~]# touch test.txt

# 修改文件权限
[root@localhost ~]# chmod 755 test.txt

# 验证文件权限是否修改成功
[root@localhost ~]# ls -l test.txt
-rwxr-xr-x. 1 root root 0 Aug 10 10:00 test.txt

# 修改文件所有者和所属组
[root@localhost ~]# chown user2:group2 test.txt

# 验证文件所有者和所属组是否修改成功
[root@localhost ~]# ls -l test.txt
-rwxr-xr-x. 1 user2 group2 0 Aug 10 10:00 test.txt

# 查看文件访问控制列表
[root@localhost ~]# getfacl test.txt

# 设置文件访问控制列表
[root@localhost ~]# setfacl -m u:user1:rw test.txt

# 验证文件访问控制列表是否设置成功
[root@localhost ~]# getfacl test.txt
# file: test.txt
# owner: user2
# group: group2
user::rwx
user:user1:rw-
group::r-x
mask::rwx
other::r-x
```

### 系统进程管理

| 命令       | 描述                                                         | 备注     |
| ---------- | ------------------------------------------------------------ | -------- |
| systemctl  | 系统服务管理工具，用于启动、停止、重启、查询系统服务的状态等操作。 |          |
| journalctl | 系统日志管理工具，用于查看系统日志。                         |          |
| ps         | 进程管理工具，用于列出当前系统中所有进程的信息。             |          |
| pstree     | 进程树管理工具，用于以树形结构显示当前系统中所有进程及其关系。 |          |
| uptime     | 系统负载监控工具，用于显示系统的运行时间、平均负载等信息。   |          |
| free       | 内存管理工具，用于显示系统的内存使用情况。                   |          |
| top        | 监控工具，用于实时显示系统中各个进程的 CPU 占用率、内存占用率等信息。 | 交互操作 |
| kill       | 进程管理工具，用于向指定进程发送 Signal 以终止该进程。       |          |

#### 命令案例（八）

```shell
# 使用 systemctl 命令启动 sshd 服务
[root@localhost ~]# systemctl start sshd
（无输出）

# 使用 journalctl 命令查看最近一小时的系统日志
[root@localhost ~]# journalctl --since "1 hour ago"
-- Logs begin at Tue 2022-01-11 10:42:51 CST, end at Wed 2022-01-12 09:42:16 CST. --

# 使用 ps 命令查看当前所有进程的快照
# `UID`：进程的用户 ID。
# `PID`：进程的 ID。
# `PPID`：父进程的 ID。
# `C`：进程的 CPU 占用率。
# `STIME`：进程的启动时间。
# `TTY`：进程所在的终端。
# `TIME`：进程占用的 CPU 时间。
# `CMD`：进程的命令行。
[root@localhost ~]# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 Apr22 ?        00:00:03 /sbin/init
root         2     0  0 Apr22 ?        00:00:00 [kthreadd]
root         3     2  0 Apr22 ?        00:00:00 [rcu_gp]
root         4     2  0 Apr22 ?        00:00:00 [rcu_par_gp]
...
# `USER`：进程的用户名。
# `PID`：进程的 ID。
# `%CPU`：进程的 CPU 占用率。
# `%MEM`：进程的内存占用率。
# `VSZ`：进程的虚拟内存大小。
# `RSS`：进程的实际内存大小。
# `TTY`：进程所在的终端。
# `STAT`：进程的状态。
# `START`：进程的启动时间。
# `TIME`：进程占用的 CPU 时间。
# `COMMAND`：进程的命令行。
[root@localhost ~]# ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.2 168980  5648 ?        Ss   Apr22   0:03 /sbin/systemd
root         2  0.0  0.0      0     0 ?        S    Apr22   0:00 [kthreadd]
root         3  0.0  0.0      0     0 ?        I<   Apr22   0:00 [rcu_gp]
root         4  0.0  0.0      0     0 ?        I<   Apr22   0:00 [rcu_par_gp]
...

# 使用 pstree 命令查看当前所有进程的树形结构
[root@localhost ~]# pstree
systemd─┬─ModemManager───2*[{ModemManager}]
        ├─NetworkManager─┬─dhclient
        │                └─2*[{NetworkManager}]
        ├─abrt-watch-log
        ├─abrtd
        ├─acpid
        ├─atd
        ...

# 使用 uptime 命令查看系统运行时间和平均负载
[root@localhost ~]# uptime
 10:42:51 up 1 day,  3:23,  2 users,  load average: 0.00, 0.01, 0.05

# 使用 free 命令查看系统内存使用情况
[root@localhost ~]# free -h
              total        used        free      shared  buff/cache   available
Mem:           7.6G        1.8G        4.6G        192M        1.2G        5.4G
Swap:            0B          0B          0B

# 使用 top 命令实时显示系统资源使用情况，q 退出
[root@localhost ~]# top

# 使用 kill 命令向指定进程发送信号（假设进程 ID 为 12345）
[root@localhost ~]# kill -SIGTERM 12345
```

### 软件包管理

| 命令       | 描述                             | 备注                 |
| ---------- | -------------------------------- | -------------------- |
| rpm        | 安装、卸载和查询 RPM 包          |                      |
| yum        | 软件包管理器                     | 可以自动解决依赖关系 |
| createrepo | 创建 YUM 软件仓库                |                      |
| mount      | 用于挂载访问设备到文件系统的命令 |                      |

#### 命令案例（九）

```shell
# 挂载光盘到 mnt 目录下
[root@localhost ~]# mount /tmp/centos7.9-2009.iso /mnt/

# 安装软件包
[root@localhost ~]# rpm -ivh /mnt/Packages/tree-1.6.0-10.el7.x86_64.rpm 
Preparing...                          ################################# [100%]
Updating / installing...
   1:tree-1.6.0-10.el7                ################################# [100%]
   
# yum 可以解决依赖关系
[root@localhost ~]# yum install httpd
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
Resolving Dependencies
--> Running transaction check
---> Package httpd.x86_64 0:2.4.6-95.el7.centos will be installed
... ...

# 创建自定义 yum 仓库
[root@localhost ~]# mkdir /var/www/html/localrepo
[root@localhost ~]# createrepo --update /var/www/html/localrepo
Saving Primary metadata
Saving file lists metadata
Saving other metadata
Generating sqlite DBs
Sqlite DBs complete
[root@localhost ~]# systemctl enable --now httpd

# yum 客户端配置 .repo 文件的基本格式如下：
[root@client ~]# vim /etc/yum.repos.d/local.repo
[LocalRepo]    # 仓库名称，可以任意取名。
name=Repository Name  # 仓库的描述信息
baseurl=http://path/to/repository/  # 仓库的实际路径，必须可以访问
enabled=1      # 是否启用该仓库，1 表示启用，0 表示禁用
gpgcheck=0     # 是否检查 RPM 包的数字签名，1 表示检查，0 表示不检查
```

### 文件的备份与恢复

| 命令  | 描述             | 备注                                                         |
| ----- | ---------------- | ------------------------------------------------------------ |
| tar   | 打包和解压缩文件 | 可以打包多个文件或目录成为一个文件，也可以解压缩已经打包好的文件 |
| gzip  | 压缩文件         | 可以将单个文件压缩成 .gz 格式的文件                          |
| bzip2 | 压缩文件         | 可以将单个文件压缩成 .bz2 格式的文件                         |
| xz    | 压缩文件         | 可以将单个文件压缩成 .xz 格式的文件                          |

#### 命令案例（十）

```shell
# 绝对路径打包，会自动去掉 / 路径
[root@localhost ~]# tar -cf ssh1.tar /etc/ssh
tar: Removing leading `/' from member names
[root@localhost ~]# tar tvf ssh1.tar 
drwxr-xr-x root/root         0 2022-08-18 18:27 etc/ssh/
-rw-r--r-- root/root    581843 2019-08-09 09:40 etc/ssh/moduli
-rw------- root/root      3903 2022-08-18 18:27 etc/ssh/sshd_config
-rw-r--r-- root/root      2276 2019-08-09 09:40 etc/ssh/ssh_config
... ...

# 相当路径打包
[root@localhost ~]# tar -cf ssh2.tar -C /etc ssh
[root@localhost ~]# tar tvf ssh2.tar 
drwxr-xr-x root/root         0 2022-08-18 18:27 ssh/
-rw-r--r-- root/root    581843 2019-08-09 09:40 ssh/moduli
-rw------- root/root      3903 2022-08-18 18:27 ssh/sshd_config
-rw-r--r-- root/root      2276 2019-08-09 09:40 ssh/ssh_config
... ...

# 压缩 tar 包，可以在 tar 命令中直接调用压缩指令
[root@localhost ~]# gzip ssh1.tar
# 调用 gzip 压缩，必须安装 gzip
[root@localhost ~]# tar czf ssh.tar.gz /etc/ssh
# 调用 bzip2 压缩，必须安装 bzip2
[root@localhost ~]# tar cjf ssh.tar.bz2 /etc/ssh
# 调用 gzip 压缩，必须安装 xz
[root@localhost ~]# tar cJf ssh.tar.xz /etc/ssh
```

### 数据同步与网络服务

| 命令  | 描述                                             | 备注         |
| ----- | ------------------------------------------------ | ------------ |
| lftp  | 一个强大的客户端工具，支持多线程，断点续传等功能 |              |
| scp   | 用于在 Linux 系统之间复制文件和目录              |              |
| wget  | 用于从 Web 下载文件                              |              |
| curl  | 用于传输数据，支持多种协议                       |              |
| rsync | 用于在本地或远程系统之间同步文件和目录           | 支持增量备份 |

#### 命令案例（十一）

```shell
# 拷贝文件到远程主机
[root@localhost ~]# scp myfile 192.168.1.15:./
Warning: Permanently added '192.168.1.15' (ECDSA) to the list of known hosts.
root@192.168.1.11's password: 
myfile                                         100%   46KB  55.1MB/s   00:00  

# 使用客户端连接远程主机
[root@localhost ~]# lftp sftp://192.168.1.11 -u root 
Password: 
lftp root@192.168.1.11:~> ls
dr-xr-x---    5 root     root         4096 May 15 17:44 .
dr-xr-xr-x   17 root     root          244 Aug 18  2022 ..
-rw-r--r--    1 root     root        25024 May 15 16:44 ssh.tar.gz
-rw-r--r--    1 root     root        35838 May 15 16:46 ssh.tar.bz2
-rw-r--r--    1 root     root        18776 May 15 16:46 ssh.tar.xz

# 下载文件
[root@localhost ~]# wget -c ftp://192.168.1.254/ok.mp4
--2023-05-15 17:47:47--  ftp://192.168.1.254/ok.mp4
100%[================================================>] 46,363,344   168MB/s   in 0.3s  
2023-05-15 17:47:47 (168 MB/s) - ‘ok.mp4’ saved [46363344]

# 访问网站
[root@localhost ~]# curl -i http://www.baidu.com
HTTP/1.1 200 OK
Accept-Ranges: bytes
Cache-Control: private, no-cache, no-store, proxy-revalidate, no-transform
Connection: keep-alive
Content-Length: 277
Content-Type: text/html
Date: Mon, 15 May 2023 09:46:27 GMT
Etag: "575e1f60-115"
Last-Modified: Mon, 13 Jun 2016 02:50:08 GMT
Pragma: no-cache
Server: bfe/1.0.8.18

# 数据同步
[root@localhost ~]# rsync -avXSH --delete docker 192.168.1.15:/var/www/ -e 'ssh'
Warning: Permanently added '192.168.1.11' (ECDSA) to the list of known hosts.
root@192.168.1.11's password: 
sending incremental file list
docker/
docker/containerd.io-1.6.12-3.1.el8.x86_64.rpm
docker/docker-ce-20.10.21-3.el8.x86_64.rpm

sent 111,646,676 bytes  received 165 bytes  44,658,736.40 bytes/sec
total size is 111,617,772  speedup is 1.00
```

